#!/usr/bin/python3

from cache import Cache

if __name__ == '__main__':
    # Crea un objeto cache
    cache = Cache()

    # Descarga el contenido de la URL y lo almacena en la cache
    cache.retrieve('http://gsyc.urjc.es/')
    cache.retrieve('https://www.aulavirtual.urjc.es/')

    # Muestra el contenido de la URL y muestra todas las URLs en la cache
    print(cache.content('http://gsyc.urjc.es/'))
    cache.show_all()


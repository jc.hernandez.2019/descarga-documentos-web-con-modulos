#!/usr/bin/python3

from robot import Robot


# Clase cache, proporciona una cache de URLs descargadas
class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):  # Descarga el contenido de la URL y lo almacena en la cache
        if url not in self.cache:
            robot = Robot(url)
            self.cache[url] = robot.content()

    def show(self, url):  # Muestra la URL indicada en la cache
        self.retrieve(url)
        print(self.cache[url])

    def show_all(self):  # Muestra todas las URLs guardadas en la cache
        print("El listado de URLs en la cache es:")
        for url in self.cache:
            print(url)

    def content(self, url):  # Devuelve el contenido de la URL indicada en la cache
        self.retrieve(url)
        print(f"El contenido de {url} es:")
        return self.cache[url]

#!/usr/bin/python3

import urllib.request


# Clase Robot, desacarga y muestra un documento web dado por su URL
class Robot:
    def __init__(self, url):
        self.url = url
        self.retrieved = False

    def retrieve(self):  # Descarga el contenido de la URL y lo almacena en la cache si no ha sido descargado ya
        if not self.retrieved:
            print(f"Descargando {self.url}")
            f = urllib.request.urlopen(self.url)  # Descarga el contenido de la URL
            self.content = f.read().decode('utf-8')  # Almacena el contenido de la URL
            self.retrieved = True

    def show(self):  # Muestra el contenido de la URL
        self.retrieve()
        print(self.content)

    def content(self):  # Devuelve el contenido de la URL
        self.retrieve()
        return self.content
